import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { userActions, surveyActions } from "_store";
import React, { useState, useEffect } from "react";
import { Modal, Button } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";

// import { setUsername } from "../redux/actions";
export { Home };
function Home() {
  const [showModal, setShowModal] = useState(true);
  const name = useSelector((state) => state.auth.value.user.name);
  const auth = useSelector((state) => state.auth.value);
  const dispatch = useDispatch();
  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Name is required!"),
  });
  const email = useSelector((x) => x.auth.value.user.email);
  const formOptions = { resolver: yupResolver(validationSchema) };
  const { register, handleSubmit, formState } = useForm(formOptions);
  const { errors, isSubmitting } = formState;

  // function to handle form submission

  // const handleSubmit = (event) => {
  //   event.preventDefault();
  //   console.log("this is before the dispatch and username is"+JSON.stringify({name:event.target.username.value}));
  //   let username=event.target.username.value
  //   // save username to Redux state
  //   dispatch(userActions.update({name:username}));
  //   // hide modal
  //   setShowModal(false);
  // };
  const surveys = useSelector((x) => x.surveys.list?.value?.data);
  console.log("surveys " + JSON.stringify(surveys));
  useEffect(() => {
    dispatch(surveyActions.getAll());
  }, []);
  function onSubmit({ name }) {
    console.log("at the right fxn");
    // setShowModal(false);
    return dispatch(userActions.update({ name, email, auth }));
  }
  return (
    <div>
      <div className="text-center">
        <h1>Hi {name}, welcome to the survey!</h1>
        <Link to="add" className="btn btn-sm btn-success mb-2">
          Add Survey
        </Link>
        <table className="table table-striped">
          <thead>
            <tr>
              <th style={{ width: "30%" }}>Survey Title</th>
              <th style={{ width: "30%" }}>Survey Description</th>
              <th style={{ width: "30%" }}>Updated On</th>
              <th style={{ width: "10%" }}></th>
            </tr>
          </thead>
          <tbody>
            {surveys?.map((survey) => (
              <tr key={survey.id}>
                <td>{survey.surveyName}</td>
                <td>{survey.surveyDescription}</td>
                <td>{new Date(survey.updatedAt).toLocaleString("en-CA")}</td>
                <td style={{ whiteSpace: "nowrap" }}>
                  <Link
                    to={`edit/${survey.id}`}
                    className="btn btn-sm btn-primary me-1"
                  >
                    Edit
                  </Link>
                  <button
                    onClick={() => dispatch(userActions.delete(survey.id))}
                    className="btn btn-sm btn-danger"
                    style={{ width: "60px" }}
                    disabled={survey.isDeleting}
                  >
                    {survey.isDeleting ? (
                      <span className="spinner-border spinner-border-sm"></span>
                    ) : (
                      <span>Delete</span>
                    )}
                  </button>
                </td>
              </tr>
            ))}
            {surveys?.loading && (
              <tr>
                <td colSpan="4" className="text-center">
                  <span className="spinner-border spinner-border-lg align-center"></span>
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
      {!name ? (
        <Modal
          show={showModal}
          onHide={() => setShowModal(false)}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header className="items-center">
            <Modal.Title className="form-group mx-auto">
              Enter Your Name
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="form-group">
                <label htmlFor="name">Full name:</label>
                <input
                  name="name"
                  type="text"
                  className={`form-control ${errors.name ? "is-invalid" : ""}`}
                  {...register("name")}
                />
                <div className="invalid-feedback">{errors.name?.message}</div>
              </div>
              <div className="form-group text-center">
                <button
                  disabled={isSubmitting}
                  className="btn btn-primary mt-2"
                >
                  {isSubmitting && (
                    <span className="spinner-border spinner-border-sm me-1"></span>
                  )}
                  Submit
                </button>
              </div>
            </form>
          </Modal.Body>
        </Modal>
      ) : (
        ""
      )}
    </div>
  );
}
