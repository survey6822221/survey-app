import { useSearchParams } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import React, { useEffect } from "react";
import { authActions } from "_store";

export { Login };

function Login() {
  const dispatch = useDispatch();
  const validationSchema = Yup.object().shape({
    email: Yup.string().required("E-mail is required"),
  });
  const formOptions = { resolver: yupResolver(validationSchema) };
  const { register, handleSubmit, formState } = useForm(formOptions);
  const { errors, isSubmitting } = formState;
  const [searchParams] = useSearchParams();
  const emailToken = searchParams.get("emailToken");
  // const [isFirstRender, setIsFirstRender] = useState(true);
  useEffect(() => {
    if (
      typeof emailToken === "string" &&
      emailToken !== "" &&
      emailToken !== null
    ) {
      console.log(
        "type of token is" +
          typeof emailToken +
          " and the value is" +
          JSON.stringify(emailToken)
      );
      dispatch(authActions.loginToLink({ emailToken }));
    }
  }, [emailToken]);

  function onSubmit({ email }) {
    return dispatch(authActions.sendLoginLinkToEmail({ email }));
  }
  return (
    <div className="card m-3">
      <h4 className="card-header text-center">Survey App</h4>
      <div className="card-body text-left">
        <h6 className="text-center">
          Welcome to our Survey App. Please enter your email, and we will send
          your login link!
        </h6>
        <form onSubmit={handleSubmit(onSubmit)} className="text-center">
          <div className="mb-3 text-left">
            <label className="form-label text-left">E-mail</label>
            <input
              name="email"
              type="text"
              {...register("email")}
              className={`form-control text-center ${
                errors.email ? "is-invalid" : ""
              }`}
            />
            <div className="invalid-feedback">{errors.email?.message}</div>
          </div>
          <button
            disabled={isSubmitting}
            className="btn btn-primary text-center mx-auto"
          >
            {isSubmitting && (
              <span className="spinner-border spinner-border-sm me-1"></span>
            )}
            Send Login link
          </button>
        </form>
      </div>
    </div>
  );
}
