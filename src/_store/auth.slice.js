import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { alertActions } from "_store";
import { history, fetchWrapper } from "_helpers";

// create slice

const name = "auth";
const initialState = createInitialState();
const reducers = createReducers();
const extraActions = createExtraActions();
const slice = createSlice({ name, initialState, reducers });

// exports

export const authActions = { ...slice.actions, ...extraActions };
export const authReducer = slice.reducer;

// implementation

function createInitialState() {
  return {
    // initialize state from local storage to enable user to stay logged in
    value: JSON.parse(localStorage.getItem("auth")),
  };
}

function createReducers() {
  return {
    setAuth,
  };

  function setAuth(state, action) {
    console.log(
      "the state is" +
        JSON.stringify(state) +
        " the action is " +
        JSON.stringify(action)
    );
    state.value = action.payload;
  }
}

function createExtraActions() {
  const baseUrl = `${process.env.REACT_APP_API_URL}/auth/email`;
  return {
    loginToLink: loginToLink(),
    logout: logout(),
    sendLoginLinkToEmail: sendEmail(),
  };
  function loginToLink() {
    return createAsyncThunk(
      `${name}/login`,
      async function ({ emailToken }, { dispatch }) {
        console.log("emailToken is" + emailToken);
        dispatch(alertActions.clear());
        try {
          const user = await fetchWrapper.get(
            `${baseUrl}/verify/${emailToken}`
          );
          // set auth user in redux state
          console.log(JSON.stringify(user));
          if (user.status === 403) {
            dispatch(alertActions.error("Invalid Login Link!"));
            console.log("BEING REDIRECTED");
            history.navigate("/account/login");
          } else {
            console.log("the user token is" + JSON.stringify(user));
            dispatch(authActions.setAuth(user.data));
            localStorage.setItem("auth", JSON.stringify(user.data));
            const { from } = history.location.state || {
              from: { pathname: "/" },
            };
            history.navigate(from);
          }
        } catch (error) {
          dispatch(alertActions.error(error));
        }
      }
    );
  }
  function sendEmail() {
    return createAsyncThunk(
      `${name}/sendLoginLinkToEmail`,
      async function ({ email }, { dispatch }) {
        dispatch(alertActions.clear());
        try {
          await fetchWrapper.post(`${baseUrl}/register`, {
            email,
          });
          // set auth user in redux state
          // dispatch(authActions.setAuth(user));
          dispatch(
            alertActions.success(
              "We sent one-time login link to the email. Please Check!"
            )
          );
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          //localStorage.setItem("auth", JSON.stringify(resp));
          // get return url from location state or default to home page
          // const { from } = history.location.state || { from: { pathname: '/' } };
          // history.navigate(from);
        } catch (error) {
          dispatch(alertActions.error(error));
        }
      }
    );
  }

  function logout() {
    return createAsyncThunk(`${name}/logout`, function (arg, { dispatch }) {
      dispatch(authActions.setAuth(null));
      localStorage.removeItem("auth");
      history.navigate("/account/login");
    });
  }
}
