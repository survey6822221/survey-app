import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { authActions, alertActions } from "_store";
import { fetchWrapper } from "_helpers";
import { useSelector } from "react-redux";

// create slice

const name = "surveys";
const initialState = createInitialState();
const extraActions = createExtraActions();
const extraReducers = createExtraReducers();
const slice = createSlice({ name, initialState, extraReducers });

// exports

export const surveyActions = { ...slice.actions, ...extraActions };
export const surveyReducer = slice.reducer;

// implementation

function createInitialState() {
  return {
    list: null,
    item: null,
  };
}

function createExtraActions() {
  const baseUrl = `${process.env.REACT_APP_API_URL}/survey`;

  return {
    create: create(),
    getAll: getAll(),
    getById: getById(),
    update: update(),
    delete: _delete(),
  };

  function create() {
    return createAsyncThunk(
      `${name}/new`,
      async (survey) => await fetchWrapper.post(`${baseUrl}/new`, survey)
    );
  }

  function getAll() {
    return createAsyncThunk(`${name}/getAll`, async () => {
      let list = await fetchWrapper.get(baseUrl);
      console.log("the list is" + JSON.stringify(list));
      return list.data;
    });
  }

  function getById() {
    return createAsyncThunk(
      `${name}/getById`,
      async (id) => await fetchWrapper.get(`${baseUrl}/${id}`)
    );
  }

  function update() {
    return createAsyncThunk(
      `${name}/update`,
      async function ({ name, email, auth }, { dispatch }) {
        let response;
        try {
          console.log("you are sending this to server" + JSON.stringify(name));
          response = await fetchWrapper.post(`${baseUrl}/profile/update`, {
            name,
            email,
          });
          console.log("the auth is " + auth);
          if (email === auth?.user.email.toString()) {
            console.log("emails are the same");
            //   // update local storage
            console.log(")))))))))" + JSON.stringify(response.data.data));
            const user = { ...auth, ...response.data.data };
            localStorage.setItem("auth", JSON.stringify(user));
            //   // update auth user in redux state
            dispatch(authActions.setAuth(user));
          }
          return response;
        } catch (error) {
          console.log(error);
        }
        // update stored user if the logged in user updated their own record
      }
    );
  }

  // prefixed with underscore because delete is a reserved word in javascript
  function _delete() {
    return createAsyncThunk(
      `${name}/delete`,
      async function (id, { getState, dispatch }) {
        await fetchWrapper.delete(`${baseUrl}/${id}`);

        // auto logout if the logged in user deleted their own record
        if (id === getState().auth.value?.id) {
          dispatch(authActions.logout());
        }
      }
    );
  }
}

function createExtraReducers() {
  return (builder) => {
    getAll();
    getById();
    _delete();

    function getAll() {
      var { pending, fulfilled, rejected } = extraActions.getAll;
      builder
        .addCase(pending, (state) => {
          state.list = { loading: true };
        })
        .addCase(fulfilled, (state, action) => {
          state.list = { value: action.payload };
        })
        .addCase(rejected, (state, action) => {
          state.list = { error: action.error };
        });
    }

    function getById() {
      var { pending, fulfilled, rejected } = extraActions.getById;
      builder
        .addCase(pending, (state) => {
          state.item = { loading: true };
        })
        .addCase(fulfilled, (state, action) => {
          state.item = { value: action.payload };
        })
        .addCase(rejected, (state, action) => {
          state.item = { error: action.error };
        });
    }

    function _delete() {
      var { pending, fulfilled, rejected } = extraActions.delete;
      builder
        .addCase(pending, (state, action) => {
          const user = state.list.value.find((x) => x.id === action.meta.arg);
          user.isDeleting = true;
        })
        .addCase(fulfilled, (state, action) => {
          state.list.value = state.list.value.filter(
            (x) => x.id !== action.meta.arg
          );
        })
        .addCase(rejected, (state, action) => {
          const user = state.list.value.find((x) => x.id === action.meta.arg);
          user.isDeleting = false;
        });
    }
  };
}
